import { setDataInStorageV2, getDataFromStorage } from "../storage/storage";

export function saveData(entityName, body) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => save(entityName, body));
    };
}

export function updateData(entityName, body) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => update(entityName, body));
    };
}
export function getAllData(entityName, params) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => getAll(entityName, params));
    };
}

export function save(entityName, data){
  let values = getDataFromStorage(entityName);
  values.push(data);
  setDataInStorageV2(entityName, values);
  return true;
}

export function update(entityName, data){
    let values = getDataFromStorage(entityName);
    let newValues = [];
    values.forEach((val) => {
        if(val["id"] !== data["id"]){
            newValues.push(val);
        }
    })

    newValues.push(data);

    setDataInStorageV2(entityName, newValues);
    return true;
}

export function getAll(entityName, data){
    let values = getDataFromStorage(entityName);
    let total = values.length;
    let { page, pageSize, sortCol, orderBy, searchText } = data;

    if(searchText !== undefined && searchText !== ""){
        values = searchInObject(values, searchText);
        total = values.length;
    }

    if(sortCol === undefined || sortCol === ""){
        sortCol = "id";
    }
    if(orderBy === undefined || orderBy === ""){
        orderBy = "DESC";
    }

    values = values.sort(sortData(sortCol, orderBy)); 
   
    if(page === undefined){
        page = 1;
    }
    if(pageSize === undefined){
        pageSize = 2;
    }
    let a = page * pageSize;
    let resultValues = values.slice( ( a - pageSize) , a);
    return { "totalResults" : total, "resultValues" : resultValues };
}

export function removeDataById(entityName, id){
    let values = getDataFromStorage(entityName);
    let newValues = [];
    values.forEach((val) => {
        if(val["id"] !== id ){
            newValues.push(val);
        }
    })

    setDataInStorageV2(entityName, newValues);
    return true;
}

export function removeById(entityName, id) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => removeDataById(entityName, id));
    };
}

export function sortData(prop, orderby) { 
    let val1 = "", val2 = "";   
    return function(a, b) {   
        val1 = typeof a[prop] === "string" ? a[prop].toUpperCase() : a[prop];
        val2 =  typeof b[prop] === "string" ? b[prop].toUpperCase() : b[prop];
        if (orderby === 'DESC'){
            if (val1 > val2) {    
                return -1;    
            } else if (val1 < val2) {    
                return 1;    
            }   
        }else{
            if (val1 > val2) {    
                return 1;    
            } else if (val1 < val2) {    
                return -1;    
            }    
        }
        
        return 0;    
    }    
}  

export function searchInObject(data, value){
    let results = [];
    value = value.toUpperCase();
    
    for(var i = 0; i < data.length; i++) {
        for(let key in data[i]) {
            
            let val = data[i][key].toString().toUpperCase();

            if(val.indexOf(value) !== -1) {
                results.push(data[i]);
                break;
            }
            
        }
    }

    return results;
}