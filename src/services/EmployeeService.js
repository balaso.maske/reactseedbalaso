import axios from "axios";
import * as employeeAction from "../../src/store/action/employeeAction";
import * as commonAction from "../../src/store/action/commonAction";
import { SuccessToastr } from '../components/Toastr';
import { getFromStorage, setDataInStorage } from "../storage/storage";

  export function fetchEmployees() {
    return dispatch => {
     /** dummy call */
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => getFromStorage());
      /*dispatch(commonAction.APICallBegin());
       return axios.get(`https://dummy.restapiexample.com/api/v1/employees`)
        .then(resp => {
          dispatch(commonAction.APICallEnd());
          return resp.data.data;
        })
        .catch(error =>
          dispatch(commonAction.APICallError(error))
        );*/
    };
  }

  export function removeEmployeeById(id) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => removeData(id));
      /*
        dispatch(commonAction.APICallBegin());
      let url = "https://dummy.restapiexample.com/public/api/v1/delete/" + id;
      return axios.delete(url)
        .then(resp => {
          dispatch(employeeAction.deleteEmployeeById(resp.data));
          dispatch(commonAction.APICallEnd());
          SuccessToastr("Successfully Deleted Employee ID : "+ id);
          return resp.data;
        })
        .catch(error =>
          dispatch(commonAction.APICallError(error))
        );*/
    };
  }
export function saveData(employee){
  let id = Date.now();
  employee["id"] =id;
  var date = new Date();
  employee["date_updated"] = date.toISOString();
let employees = getFromStorage();
employees.push(employee);
setDataInStorage(employees);
return id;
}

export function removeData(id){
let employees = getFromStorage();
let newEmployees = [];
employees.forEach((employee) => {
  if(employee["id"] !== id){
    newEmployees.push(employee);
  }
})
setDataInStorage(newEmployees);
return id;
}
export function updateData(newEmployee){
  let employees = getFromStorage();
  let newEmployees = [];
  employees.forEach((employee) => {
    if(employee["id"] !== newEmployee["id"]){
      newEmployees.push(employee);
    }
  })
  var date = new Date();
  newEmployee["date_updated"] = date.toISOString();
  newEmployees.push(newEmployee);

  setDataInStorage(newEmployees);
  return newEmployee["id"];
}


  export function saveEmployee(employee) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => saveData(employee));
        /*
         dispatch(commonAction.APICallBegin());
      let url = "https://dummy.restapiexample.com/api/v1/create";
      return axios.post(url, employee, axiosConfig, { crossDomain: true })
        .then(resp => {
          dispatch(commonAction.APICallEnd());
        //  SuccessToastr("Successfully Saved);
          return resp.data;
        })
        .catch(error =>
          dispatch(commonAction.APICallError(error))
        );*/
    };
  }

  export function updateEmployee(employee) {
    return dispatch => {
      const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
      return wait(1*1000).then(() => updateData(employee));
      /*
        dispatch(commonAction.APICallBegin());
      let url = "https://dummy.restapiexample.com/api/v1/update/"+ employee.id;
      return axios.put(url, employee, axiosConfig, { crossDomain: true })
        .then(resp => {
          dispatch(commonAction.APICallEnd());
        //  SuccessToastr("Successfully Saved);
          return resp.data;
        })
        .catch(error =>
          dispatch(commonAction.APICallError(error))
        );*/
    };
  }
  
  
  // Handle HTTP errors since fetch won't.
  function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }

export const request_header = {
        headers: {
          'Content-Type':'application/json'
      }
  };

  export const axiosConfig = {
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Access-Control-Allow-Origin": "*",
    }
  };

