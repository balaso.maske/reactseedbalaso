import * as commonAction from "../../src/store/action/commonAction";
import axios from "axios";
const queryString = require('query-string');

const serverUrl = "https://balasomaske.herokuapp.com/";

export function setSelectedEntity(entity) {
    return dispatch => {
      dispatch(commonAction.setEntity(entity));
    };
}

export function toggleLeftMenu(status) {
  return dispatch => {
    dispatch(commonAction.toggleLeftMenu(status));
  };
}

export function isUserAuthenticate(data) {
  return dispatch => {
    dispatch(commonAction.isUserAuthenticate(data));
  };
}

export function showAdminView(val) {
  return dispatch => {
    dispatch(commonAction.showAdminView(val));
  };
}

export function getAll(entityName, pageNo, pageSize, searchText, sortCol, sortOrder, status) {
  var start = (pageNo - 1) * pageSize;
  var params = {};
  params.start = start;
  params.length = pageSize;
  params.lmiView = "own";
  params.searchTagName = "";
  if (searchText) {
      params.filterText = "%" + searchText + "%";
  }
  if (sortCol){
    params.sortCol = sortCol;
  }
  if (sortOrder){
    params.sortOrder = sortOrder;
  }
  if (status){
    params.status = status;
  }
  params = queryString.stringify(params);
  let url = serverUrl + "services/session/" + entityName + "/getAll";
  url = url + "?"+ params;
  return dispatch => {
    dispatch(commonAction.APICallBegin());
    return axios.get(url)
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      })
      .catch(error =>
        dispatch(commonAction.APICallError(error))
      );
  };
};

export function save (entityName, entity) {
    let url = serverUrl + "services/session/" + entityName;
    return dispatch => {
      dispatch(commonAction.APICallBegin());
    return axios.post(url, entity, axiosConfig, { crossDomain: true })
      .then(resp => {
        dispatch(commonAction.APICallEnd());
      //  SuccessToastr("Successfully Saved);
        return resp.data;
      })
      .catch(error =>
        dispatch(commonAction.APICallError(error))
      );
  };
}

export function deleteEntry(entityName, entityId) {
  let url = serverUrl + "services/session/" + entityName + "/delete/" + entityId;
  return dispatch => {
    dispatch(commonAction.APICallBegin());
    return axios.get(url)
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      })
      .catch(error =>
        dispatch(commonAction.APICallError(error))
      );
  };
}

export function login (credentials) {
  let url = serverUrl + "j_spring_security_check";
  let param = {
        'j_username': credentials.username,
        'j_password': credentials.password,
        '_spring_security_remember_me': credentials.remember
    }
    param = queryString.stringify(param);
    return dispatch => {
      dispatch(commonAction.APICallBegin());
    return axios.post(url, param, request_header)
      .then(resp => {
        dispatch(commonAction.APICallEnd());
      //  SuccessToastr("Successfully Saved);
        return resp.data;
      })
      .catch(error =>
        dispatch(commonAction.APICallError(error))
      );
  };
};

export function getNews({ newsType, country, page , pageSize }){
  let param = {}
  param.page = page;
  param.pageSize = pageSize;
  if(newsType === 'top-headlines'){
    param.country = country;
  }
  param = queryString.stringify(param);
  
  if(newsType === 'everything'){
    param = param + "&q=*";
  }
  return dispatch => {
    dispatch(commonAction.APICallBegin());
    let url = "https://newsapi.org/v2/" + newsType + "?" + param;
   
 return axios({
        "method":"GET",
        "url": url,
        mode: 'no-cors',
        "headers":{
        "content-type":"application/x-www-form-urlencoded",
        "X-Api-Key" : "03ea8fd2ffba454b8d0a185dbf1290b7",
        }      
        })
        .then((response)=>{
          dispatch(commonAction.APICallEnd());
          return response.data;
        })
        .catch((error)=>{
          console.log(error)
        })
  }

}

export function registration (data) {
  let url = serverUrl + "api/register";
  return dispatch => {
      dispatch(commonAction.APICallBegin());

      
    return axios.post(url, data, requestHeader)
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      }).catch(error => {
        if(error.response){
          dispatch(commonAction.APICallEnd());
          return error.response.data;
        }
        return dispatch(commonAction.APICallError(error));
      });
  };
}

export function resetPassword (data) {
  let url = serverUrl + "api/account/reset-password/init";
  return dispatch => {
      dispatch(commonAction.APICallBegin());
    return axios.post(url, data, requestHeader)
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      }).catch(error => {
        if(error.response){
          dispatch(commonAction.APICallEnd());
          return error.response.data;
        }
        return dispatch(commonAction.APICallError(error));
      });
  };
}

export function resetPasswordFinish(data) {
  let url = serverUrl + "api/account/reset-password/finish";
  return dispatch => {
      dispatch(commonAction.APICallBegin());
    return axios.post(url, data, requestHeader)
      .then(resp => {
        dispatch(commonAction.APICallEnd());
        return resp.data;
      }).catch(error => {
        if(error.response){
          dispatch(commonAction.APICallEnd());
          return error.response.data;
        }
        return dispatch(commonAction.APICallError(error));
      });
  };
}


export const numberFormat = value =>
new Intl.NumberFormat("en-IN", {
  style: "currency",
  currency: "INR"
}).format(value);


export const request_header = {
    headers: {
      'Content-Type':'application/x-www-form-urlencoded',
      "Access-Control-Allow-Origin": "*",
      "Cookie": "JSESSIONID=1B02EE859EDBC01BFF6A9F546C7736AA; _ga=GA1.1.1775654070.1599374480; SL_G_WPT_TO=en; SL_GWPT_Show_Hide_tmp=undefined; SL_wptGlobTipTmp=undefined; _gid=GA1.1.1222287709.1599648106"
  }
};

export const requestHeader = {
  headers: {
    'Content-Type':'application/json',
    "Access-Control-Allow-Origin": "*"
  }
};

export const axiosConfig = {
  headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "Access-Control-Allow-Origin": "*",
      "Cookie": "JSESSIONID=1B02EE859EDBC01BFF6A9F546C7736AA; _ga=GA1.1.1775654070.1599374480; SL_G_WPT_TO=en; SL_GWPT_Show_Hide_tmp=undefined; SL_wptGlobTipTmp=undefined; _gid=GA1.1.1222287709.1599648106"
  }
};