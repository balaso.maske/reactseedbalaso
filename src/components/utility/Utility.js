export function InputValidation(inputObject, inputValue){
    if(inputObject.required){
        if(inputObject.inputType === "email"){
            return emailValidation(inputObject, inputValue);
        }else if(inputValue !== ""){
         return true;   
        }else{
            return false;
        }
        
    }else{
        if(inputObject.inputType === "email" && inputValue !== ""){
            return emailValidation(inputObject, inputValue);
        }
        return true;
    }
}

export function emailValidation(inputObject, inputValue){
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(inputValue)) {
        return false;
    }else{
        return true;
    }
}