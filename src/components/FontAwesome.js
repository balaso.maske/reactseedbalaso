import React from "react";
import * as FontAwesome from "react-icons/fa";

export const Icon = props => {
  const { iconName, size, color } = props;
  const icon = React.createElement(FontAwesome[iconName]);
  return (
    <span style={{ fontSize: size, color: color }}>{icon}</span>
  );
};