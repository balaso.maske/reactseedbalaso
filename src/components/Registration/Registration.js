import React, {Component} from "react";
import {connect} from "react-redux";

import * as CommonService from "../../services/CommonService";
import { Icon } from '../FontAwesome';
import Footer from '../footer/footer';
import TextInput from '../Form/textInput';
import { SuccessToastr } from '../Toastr';
import { InputValidation } from '../utility/Utility';

class Registration extends Component {

    constructor(props){
        super(props);
        
        this.formData = [
            {
              "name": "username",
              "value": "",
              "required": true,
              "regex": "",
              "label": "User Name",
              "inputType": "text",
              "errorMsg": "Please enter valid User Name"
            },
            {
              "name": "email",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Email",
              "inputType": "email",
              "errorMsg": "Please enter valid Email"
            },
            {
              "name": "password",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Password",
              "inputType": "password",
              "errorMsg": "Please enter valid Password"
            
            }
        ];
        let object = {};
        this.formData.forEach((input) => {
            let name = input["name"];
            object[name] =  input["value"];
        });
        this.state = object;
    }
    componentDidMount(){
        this.setState({error: ""});
    }
    onValueChange = (event) => {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({ [name]: value });
    };
    goTo(pageName){
        this.props.transition.router.stateService.go(pageName);
    }

    handleClick = (e) => {
    e.preventDefault();
    this.goTo(e.target.name);
    };
  

    register = (event) =>{
        let isValid = true;
        let inputName = {};
       for (let i = 0; i < this.formData.length; i++) {
           let input = this.formData[i];
           isValid = InputValidation(input, this.state[input.name]);
           if(isValid === false){
               console.log(input);
               inputName = input.name;
               break;
           }
        }
        if(isValid){
            this.setState({ errorInput : "" })
            let dataObject = {};
            for (let i = 0; i < this.formData.length; i++) {
                let input = this.formData[i];
                dataObject[input.name] = this.state[input.name]
            }
            let response = this.props.dispatch(CommonService.registration(dataObject));
            response.then(value => {
                debugger;
                if(value.success === true){
                    SuccessToastr("Registration Completed Successfully");
                    let { transition } = this.props;
                    transition.router.stateService.go('login');
                }else{
                   this.setState({error: value.message});
                }
              }, reason => {
                  debugger;
                console.error(reason); // Error!
              });
         }else{
             this.setState({ errorInput : inputName })
         }
    }

    render() {
        const isError = false, errorMsg = "";
        return (
            <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div className="card-signin my-5">
                <div className="signup-form">
   <form className="border border-light ">
            <h2>Register</h2>
            <p className="hint-text">Create your account. It's free and only takes a minute.</p>
      { 
        this.state.error !== "" &&
        <div className="alert alert-danger ng-binding" role="alert">
            <strong>Error!</strong> {this.state.error}
        </div>
        }

      { this.formData.map(input => (
                    <TextInput label={input.label} isError={ input.name === this.state.errorInput ? true : false }onValueChange={this.onValueChange} key={input.name} isEdit={false} isEditModeDisplay={input.isEditModeDisplay}
                    isDisabled={input.isDisabled} name={input.name} value={this.state[input.name]} inputType={input.inputType} required={input.required} errorMsg={input.errorMsg} />
        ))}
      <div className="form-group">
      <label className="required">Confirm Password </label>
          <input type="password" className="form-control" placeholder="Confirm Password" name="confirmPassword"
          onChange={this.onValueChange} required value={this.state.confirmPassword} />
          { isError &&
                <div className="invalid-feedback">
                {errorMsg}
              </div>
                }
      </div>
      <div className="form-group">
			<label className="form-check-label">
                <input type="checkbox" required="required" />
                 &nbsp; I accept the <a href="#">Terms of Use</a> &amp; &nbsp;
                 <a href="#">Privacy Policy</a></label>
		</div>
      
      <button className="btn btn-success btn-lg btn-block"
       type="submit" onClick={(e)=>{this.register()}}>Register Now</button>
        <br/>
    <div className="text-center">Already have an account? 
        <a href="#"  onClick={this.handleClick} name="login">Sign in</a>
    </div>
   </form>
</div>
</div>
</div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
      loading: state.commonReducer.loading,
      isAuthenticated : state.commonReducer.isAuthenticated,
    }
  };

export default connect(mapStateToProps)(Registration);