import React, {Component} from 'react';
import {connect} from "react-redux";
import { Icon } from '../FontAwesome';
import * as EntityService from "../../services/EntityService";
import * as CommonService from "../../services/CommonService";
import Moment from 'react-moment';
import { FaTrash, FaEdit } from 'react-icons/fa';
import { SuccessToastr } from '../Toastr';
import Pagination from '@material-ui/lab/Pagination';
import TableHeader from '../Table/TableHeader';

class User extends Component {
    constructor(props){
        super(props);
        this.state = {
            "pageName": "User",
            pageSize : 2,
            page: 1,
            sortCol: "",
            sortReverse: true,
            searchText : "",
            data : { total : 0, data:[]}
        }

        this.onSortChange = this.onSortChange.bind(this);

        this.tableHeaders = [
          {
            "sortColumnName": "name",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "Name"
          },
          {
            "sortColumnName": "email",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "Email"
          },
          {
            "sortColumnName": "date_updated",
            "isDisplay": true,
            "isSortBySupport": true,
            "displayName": "Updated"
          },
          {
            "sortColumnName": "",
            "isDisplay": true,
            "isSortBySupport": false,
            "displayName": "Action"
          }
        ];
    }
    onSortChange(sortColumn){
       this.setState({ sortCol : sortColumn, sortReverse : !this.state.sortReverse}, () => {
        this.fetchData();
       }); 
    }

    goTo(page){
        let { transition } = this.props;
        transition.router.stateService.go(page);
    }
  
  editData(user){
      let object = {
        "name": this.state.pageName,
        "data": user
      }
      this.props.dispatch(CommonService.setSelectedEntity(object));
      this.goTo('userEntry');
  }

    deleteData(user){
      let response = this.props.dispatch(EntityService.removeById(this.state.pageName, user.id));
      response.then(value => {
        SuccessToastr("User Deleted Successfully");
          this.fetchData();
      }, reason => {
        console.error(reason); // Error!
      });
    }

    componentDidMount() {
      let object = {
        "name": this.state.pageName,
        "data": undefined
      }
      this.props.dispatch(CommonService.setSelectedEntity(object));
      this.fetchData();
    }
  
    fetchData(){
      let object = {
        page : this.state.page,
        pageSize : this.state.pageSize,
        sortCol : this.state.sortCol,
        orderBy: this.state.sortReverse ? "DESC" : "ASC",
        searchText : this.state.searchText
      }
      let response = this.props.dispatch(EntityService.getAllData(this.state.pageName, object));
      response.then(res => {
        this.setState({ data : { total: res.totalResults, data: res.resultValues}});
      }, reason => {
        console.error(reason); // Error!
      });
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value, page : 1 }, () => {
          this.fetchData();
        }); 
    };

    _handleSearchTextChange(e) {
      this.setState({ searchText : e.target.value });
   }

  pageHandleChange = (event, value) => {
      this.setState({ page : value }, () => {
          this.fetchData();
      }); 
  };

  handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      this.fetchData();
    }
  }
      
  render(){
        const { isAdmin, isAdminView } = this.props;
        const { data } = this.state.data;
    return(
        <div>
      <div className="well well-sm">
         <div className="row">
               <button type="button" className="col-sm-2 col-md-2 col-lg-2 btn btn-success"  
                  onClick={() => this.goTo('userEntry')} style={{marginLeft : 1 + 'em'}}>
                    New User  <Icon iconName="FaPlus"/>
               </button>
            <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 searchBTn">
          <div class="has-feedback">
              <input type="text" class="form-control borderradius" id="search" name="searchText"
                  placeholder="Search" onKeyDown={this.handleKeyDown} value={this.state.searchText}
                  onChange={event => { this.setState({ searchText: event.target.value})}} />
            </div>
		</div>
           
         </div>
      </div>
      <div>
      <div className="table-responsive">
         <table className="table table-striped table-hover">
            <thead>
            
               <tr>
                  { this.tableHeaders.map(header => (
                      <TableHeader key={header.sortColumnName} displayName={header.displayName} onSortChange={this.onSortChange} sortColumnName={header.sortColumnName} sortCol={this.state.sortCol} sortReverse={this.state.sortReverse} isSortBySupport={header.isSortBySupport} />
                  ))}
               </tr>
            </thead>
            <tbody>
               { data.map(user => (
               <tr className="clickableElement" key={user.id} onDoubleClick={() => this.editData(user)}>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>
                    <span>{user.updated_by} </span>
                      <div className="text-primary-mute">
                        <Moment fromNow>{user.date_updated}</Moment>
                      </div> 
                    </td>
                 
                  <td className="chartFeatures"> 
                  <FaTrash onClick={() => this.deleteData(user)}> Delete </FaTrash>
                     &nbsp;
                     <FaEdit onClick={() => this.editData(user)}> Edit </FaEdit>
                  </td>
               </tr>
               ))
               }
            </tbody>
         </table>
      </div>
      <div>
      <div className="row">
                <div className="col-sm-10 col-md-10">
                  <div className="form-group ">
                      <label htmlFor="page">Page: {this.state.page}</label>
                      <Pagination variant="outlined" siblingCount={0}
                      size="large" color="primary"
                        count={Math.ceil(this.state.data.total / this.state.pageSize)} 
                        page={this.state.page} name="page" onChange={this.pageHandleChange} />
                  </div>
              </div>
              <div className="col-sm-2 col-md-2">
                    <label htmlFor="PageSize">Page Size</label>
                    <select className="form-control" value={this.state.pageSize} name="pageSize" onChange={this.handleChange}>
                                <option value="2">2</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                        </select>
              </div>
            </div>
      </div>
   </div>
   </div>
    )
    }
}

const mapStateToProps = state => ({
    isAdmin : state.commonReducer.isAdmin,
    isAdminView : state.commonReducer.isAdminView
});

export default connect(mapStateToProps)(User);