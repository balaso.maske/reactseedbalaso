import React, { Component } from "react";
import {connect} from "react-redux";
import * as CommonService from "../../services/CommonService";
import * as EntityService from "../../services/EntityService";
import { Icon } from '../FontAwesome';
import { SuccessToastr } from '../Toastr';
import  CustomForm from "../Form/form";

class UserEntry extends Component{
    constructor(props){
        super(props);
        this.state = {
            pageName: "User",
            isEdit : false
        }
        this.saveUser = this.saveUser.bind(this)
        this.formData = [
            {
                "name": "id",
                "value": "",
                "required": false,
                "regex": "",
                "label": "ID",
                "inputType": "text",
                "errorMsg": "",
                "isEditModeDisplay": true,
                "isDisabled": true
            },
            {
              "name": "name",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Name",
              "inputType": "text",
              "errorMsg": "Please enter valid data"
            },
            {
              "name": "email",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Email",
              "inputType": "email",
              "errorMsg": "Please enter valid data"
            },
            {
              "name": "password",
              "value": "",
              "required": true,
              "regex": "",
              "label": "Password",
              "inputType": "password",
              "errorMsg": "Please enter valid data"
            }
          ]
    }

    saveUser (user) { 
        var date = new Date();
        user["date_updated"] = date.toISOString();
        if(this.state.isEdit){
          var data = { name : user.name,
            email: user.email, 
            password : user.password,
             id : user.id, updated_by : this.props.user.email,
             date_updated: user.date_updated 
            };

            let response = this.props.dispatch(EntityService.updateData(this.state.pageName, data));
              response.then(value => {
                SuccessToastr(" User Updated Successfully");
                this.goTo();
              }, reason => {
                console.error(reason); // Error!
              });
        }else{
          let data = { name : user.name,
            email: user.email, 
            password : user.password,
             id : Date.now(), updated_by : this.props.user.email,
             date_updated: user.date_updated 
            };
            let response = this.props.dispatch(EntityService.saveData(this.state.pageName, data));
            response.then(value => {
              SuccessToastr(" User Saved Successfully");
              this.goTo();
            }, reason => {
              console.error(reason); // Error!
            });
        }
    }
      goTo(){
        let { transition } = this.props;
        transition.router.stateService.go('user');
      }
     
      componentWillMount(){
        const { entity } = this.props;
        if(entity && entity.name === "User" && entity.data !== undefined){
            const {data} = entity;
            if(data){
                const { id, name, email, password } = data;
              this.formData.forEach((input) => {
                let inputName = input["name"];
                    
                  if( inputName === "name"){
                    input["value"] = name;
                  }else if( inputName === "email"){
                    input["value"] = email;
                  }else if( inputName === "password"){
                    input["value"] = password;
                  }else if( inputName === "id"){
                    input["value"] = id;
                  }
              }); 
            }
            this.setState({ isEdit : true });
        }else{
          let object = {
            "name": this.state.pageName,
            "data": undefined
          }
          this.setState({ isEdit : false });
          this.props.dispatch(CommonService.setSelectedEntity(object));
        }
      }
  
    

render() {
      return (
        <div>
        <h3> <a className="link" onClick={() => this.goTo()}> { this.state.isEdit === true ? 'Update' : 'New'} User  <Icon iconName="FaRegArrowAltCircleLeft"/> </a> </h3>
        <div className="offset-md-2">
           <div className="row">
              <div className="col-md-6 col-sm-6 col-xs-12">
              <CustomForm formData={this.formData} onSave={this.saveUser} isEdit = {this.state.isEdit} />
              </div>
           </div>
        </div>
     </div>
      )
    }
}

const mapStateToProps = state => ({
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  
export default connect(mapStateToProps)(UserEntry);