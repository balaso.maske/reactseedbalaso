import React, {Component} from 'react';
import {connect} from "react-redux";


class WelcomePage extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const { isAdmin, isAdminView } = this.props;
    return(
        <h2> Welcome to { isAdminView ? 'Admin' : 'User' } Page </h2>
    )
    }
}

const mapStateToProps = state => ({
    isAdmin : state.commonReducer.isAdmin,
    isAdminView : state.commonReducer.isAdminView
});

export default connect(mapStateToProps)(WelcomePage);