import React  from 'react';
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


export const SuccessToastr = (message) => {
    toast.success(message);
};

export const ErrorToastr = (message) => {
    toast.error(message);
};

export const WarnToastr = (message) => {
    toast.warn(message);
};

export const InfoToastr = (message) => {
    toast.info(message);
};
