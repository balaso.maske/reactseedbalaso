import React, {Component} from "react";
import {connect} from "react-redux";

import * as CommonService from "../../services/CommonService";
import { Icon } from '../FontAwesome';
import Footer from '../footer/footer';
import TextInput from '../Form/textInput';
import { SuccessToastr } from '../Toastr';
import { InputValidation } from '../utility/Utility';

class ResetPasswordInit extends Component {

    constructor(props){
        super(props);
        this.formData = [
            {
              "name": "email",
              "value": "",
              "required": true,
              "regex": "",
              "label": "",
              "inputType": "text",
              "errorMsg": "Please enter valid User Name / Email ID"
            }];
            let object = {};
            this.formData.forEach((input) => {
                let name = input["name"];
                object[name] =  input["value"];
            });
            this.state = object;
    }
    onValueChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
  
        this.setState({ [name]: value });
    };
    componentDidMount(){
        this.setState({error: ""});
    }
    goTo(pageName){
        this.props.transition.router.stateService.go(pageName);
    }

    handleClick = (e) => {
        e.preventDefault();
        this.goTo(e.target.name);
    };

    resetPassword = (event) => {
        let isValid = true;
        let inputName = {};
       for (let i = 0; i < this.formData.length; i++) {
           let input = this.formData[i];
           isValid = InputValidation(input, this.state[input.name]);
           if(isValid === false){
               console.log(input);
               inputName = input.name;
               break;
           }
        }
        if(isValid){
            this.setState({ errorInput : "" })
            let dataObject = {};
            for (let i = 0; i < this.formData.length; i++) {
                let input = this.formData[i];
                dataObject[input.name] = this.state[input.name]
            }
            let response = this.props.dispatch(CommonService.resetPassword(dataObject));
            response.then(value => {
                if(value.success === true){
                    SuccessToastr(value.message);
                    let { transition } = this.props;
                    transition.router.stateService.go('login');
                }else{
                   this.setState({error: value.message});
                }
              }, reason => {
                console.error(reason); // Error!
              });
         }else{
             this.setState({ errorInput : inputName })
         }
    }

    render() {
        return (
            <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card-signin my-5">
            <div className="signup-form">
<form className="border border-light ">
        <h2>Reset Password</h2>
        <p className="hint-text">Enter your email / username below to reset your password.</p>
  { 
    this.state.error !== "" &&
    <div className="alert alert-danger ng-binding" role="alert">
        <strong>Error!</strong> {this.state.error}
    </div>
    }

  { this.formData.map(input => (
                <TextInput label={input.label} isError={ input.name === this.state.errorInput ? true : false }onValueChange={this.onValueChange} key={input.name} isEdit={false} isEditModeDisplay={input.isEditModeDisplay}
                isDisabled={input.isDisabled} name={input.name} value={this.state[input.name]} inputType={input.inputType} required={input.required} errorMsg={input.errorMsg} />
    ))}

  
  <button className="btn btn-success btn-lg btn-block"
   type="submit" onClick={(e)=>{this.resetPassword()}}>Reset Password</button>
    <br/>
<div className="text-center">
    <a href="#" onClick={this.handleClick} name="login">Sign in</a>
</div>
</form>
</div>
</div>
</div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      loading: state.commonReducer.loading,
      isAuthenticated : state.commonReducer.isAuthenticated,
    }
  };

export default connect(mapStateToProps)(ResetPasswordInit);