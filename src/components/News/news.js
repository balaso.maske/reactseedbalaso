import React, { Component } from "react";
import {connect} from "react-redux";
import * as EmployeeService from "../../services/EmployeeService";
import * as CommonService from "../../services/CommonService";
import { Icon } from '../FontAwesome';
import { SuccessToastr } from '../Toastr';
import Moment from 'react-moment';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Pagination from '@material-ui/lab/Pagination';
const queryString = require('query-string');

class News extends Component{
    constructor(props){
        super(props);
        this.state = {
            pageName: "News",
            pageSize : 2,
            page: 1,
            country: "in",
            newsType: "top-headlines",
            data : { total : 0, data:[]}
        }
    }

    componentDidMount() {
        let object = {
          "name": this.state.pageName,
          "data": undefined
        }
       this.getNewsData();
    }
    getNewsData(){
        let object = {
            newsType : this.state.newsType,
            country : this.state.country,
            page : this.state.page,
            pageSize : this.state.pageSize
        }
        let response = this.props.dispatch(CommonService.getNews(object));
        response.then(res => {
            if(res.status === "ok"){
                this.setState({ data : { total: res.totalResults, data: res.articles}});
            }else{
                this.setState({ data : { total: 0, data: [] } });
            }
          
        }, reason => {
          console.error(reason); // Error!
        });
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value, page : 1 }, () => {
            this.getNewsData();
        }); 
    };
    pageHandleChange = (event, value) => {
        this.setState({ page : value }, () => {
            this.getNewsData();
        }); 
    };
    render() {
    const { data }= this.state.data;
        return (
            <div className="container">
                <div className="well well-sm">
         <div className="row">
             <div className="col-sm-8 col-md-8 form-row">
             <div className="form-group col-md-3">
                <label htmlFor="newsType">News Type</label>
                <select className="form-control" value={this.state.newsType} name="newsType" onChange={this.handleChange}>
                            <option value="top-headlines">Top Headlines</option>
                            <option value="everything">Everything</option>
                    </select>
            </div>
         <div className="form-group col-md-3">
                <label htmlFor="country">Country</label>
                <select className="form-control" disabled={this.state.newsType === 'everything'} value={this.state.country} name="country" onChange={this.handleChange}>
                            <option value="in">India</option>
                            <option value="us">US</option>
                    </select>
            </div>
            <div className="col-sm-3 col-md-3">
                <label htmlFor="PageSize">Page Size</label>
                <select className="form-control" value={this.state.pageSize} name="pageSize" onChange={this.handleChange}>
                            <option value="2">2</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25">25</option>
                    </select>
            </div>
            </div>
            <div className="col-sm-4 col-md-4 form-row">
                <div className="form-group ">
                    <Typography>Page: {this.state.page} </Typography>
                    <Pagination variant="outlined" siblingCount={0} color="primary" count={Math.ceil(this.state.data.total / this.state.pageSize)} page={this.state.page} name="page" onChange={this.pageHandleChange} />
                </div>
            </div>
        </div>
      </div>
      
    <div className="row"> 
    { data.map( (news)  => (
        <div className="card col-sm-5" key={news.title} style={{ margin: 2.5 + 'em'}}>
           <div className="card-body">
           <img className="card-img-top" src={ news.urlToImage } alt="Card image cap"/>
                <h5 className="card-title">{ news.title } </h5>
                <p className="card-text"> { news.description } </p>
                <a target="_blank" href={ news.url}> Click Here to Read More</a>
            </div>
            <div className="card-footer">{ news.source.name }  <p className="float-right"><Moment format="YYYY-MM-DD HH:mm">{ news.publishedAt }</Moment></p></div>
        </div>
     )) }
    </div>
</div>
        )
    }
}

const mapStateToProps = state => ({
    error: state.commonReducer.error,
    entity : state.entityReducer.selectedEntity
});

export default connect(mapStateToProps)(News);