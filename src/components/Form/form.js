import React, {Component} from "react";
import TextInput from "./textInput";

class CustomForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            "errorInput" : ""
        }
        this.baseState = this.state;
    }
    onValueChange = (event) => {
        const { name, value } = event.target;
          //let {name, va} = 
          this.setState({
            [name]: value
          });
          
    };
    
    clear(){
        const { formData, isEdit } = this.props;
        let object = {};
        formData.forEach((input) => {
           object[input.name] = "";
        });

        this.setState(object);
    }

    saveData(){
    const { formData, isEdit } = this.props;
       let isValid = true;
        let inputName = {};
       for (let i = 0; i < formData.length; i++) {
           let input = formData[i];
           isValid = this.validation(input);
           if(isValid === false){
               console.log(input);
               inputName = input.name;
               break;
           }
        }
        if(isValid){
           this.props.onSave(this.state);
        }else{
            this.setState({ errorInput : inputName })
        }
    }

    validation(input){
        if(input.required){
            if(this.state[input.name] !== ""){
             return true;   
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    componentDidMount(){
        const { formData, isEdit } = this.props;
        let object = {};
        formData.forEach((input) => {
            let name = input["name"];
            object[name] =  input["value"];
        });
        this.setState(object);
    }

    render() {
       
        const { formData, isEdit } = this.props;
        return (
            <form>
            { formData.map(input => (
                    <TextInput label={input.label} isError={ input.name === this.state.errorInput ? true : false }onValueChange={this.onValueChange} key={input.name} isEdit={isEdit} isEditModeDisplay={input.isEditModeDisplay}
                    isDisabled={input.isDisabled} name={input.name} value={this.state[input.name]} inputType={input.inputType} required={input.required} errorMsg={input.errorMsg} />
            ))}
            <center>
                    <button type="button" className="btn btn-outline-secondary"
                         onClick={()=>{ this.clear()}}>Cancel</button>
                         &nbsp; &nbsp;
                       <button type="button" className="btn btn-success" onClick={(e)=>{this.saveData()}}> { isEdit ? 'Update' : 'Save'} </button> &nbsp; 
                       { isEdit === false &&
                        <button type="button" className="btn btn-warning" onClick={()=>{ this.clear()}}>Reset</button>
                        }
                    </center>
           </form>
        );
    }
}

export default CustomForm