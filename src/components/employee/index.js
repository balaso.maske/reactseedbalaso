import React, { Component } from "react";
import {connect} from "react-redux";
import * as EmployeeService from "../../services/EmployeeService";
import * as CommonService from "../../services/CommonService";
import { FaTrash, FaEdit } from 'react-icons/fa';
import { Icon } from '../FontAwesome';
import { SuccessToastr } from '../Toastr';
import Moment from 'react-moment';
class Employee extends Component{
    constructor(props){
        super(props);
        this.state = {
          "pageName": "Employee",
          sortCol:"",
          sortReverse: false,
          data : []
        }
    }
   
    componentDidMount() {
      let object = {
        "name": this.state.pageName,
        "data": undefined
      }
      this.props.dispatch(CommonService.setSelectedEntity(object));
      this.fetchData();
    }

    fetchData(){
      let response = this.props.dispatch(EmployeeService.fetchEmployees());
      response.then(value => {
        this.setState({ data: value })
      }, reason => {
        console.error(reason); // Error!
      });
    }
    getData(){
        console.log(this.props); 
    }
    editData(employee){
      let object = {
        "name": this.state.pageName,
        "data": employee
      }
      this.props.dispatch(CommonService.setSelectedEntity(object));
      this.goTo('saveEmployee');
    }
    deleteData(employee){
      let response = this.props.dispatch(EmployeeService.removeEmployeeById(employee.id));
      response.then(value => {
        SuccessToastr("User Deleted Successfully ID "+ value);
          this.fetchData();
      }, reason => {
        console.error(reason); // Error!
      });
    }

    goTo(page){
      let { transition } = this.props;
      transition.router.stateService.go(page);
    }

    onSortChange(sortCol){

    }

render() {
  const { error, loading, employees } = this.props;
  const dateToFormat = new Date();
    return (
      <div>
      <div className="well well-sm">
         <div className="row">
            <div className="col-sm-9 col-md-9 col-lg-9">
               <button type="button" className="btn btn-success"  
                  onClick={() => this.goTo('saveEmployee')} >
                    Add Employee  <Icon iconName="FaPlus"/>
               </button>
            </div>
         </div>
      </div>
      <div>
      <div className="table-responsive">
      
         <table className="table table-striped table-hover">
            <thead>
               <tr>
                  <th>Id</th>
                   <th onClick={this.onSortChange('name')} className="clickableElement">
                    <span>Name</span>
                    <span>
                      {
                        this.state.sortCol !== 'name' &&
                        <Icon iconName="FaSort"/>
                      }
                      {
                        ( this.state.sortCol === 'name' && !this.state.sortReverse) &&
                        <Icon iconName="FaCaretDown"/>
                      }
                      {
                        (this.state.sortCol === 'name' && this.state.sortReverse ) &&
                        <Icon iconName="FaCaretUp"/>
                      }
                    </span>
                </th>
                <th onClick={this.onSortChange('age')} className="clickableElement">
                    <span>Age</span>
                    <span>
                      {
                        this.state.sortCol !== 'age' &&
                        <Icon iconName="FaSort"/>
                      }
                      {
                        ( this.state.sortCol === 'age' && !this.state.sortReverse) &&
                        <Icon iconName="FaCaretDown"/>
                      }
                      {
                        (this.state.sortCol === 'age' && this.state.sortReverse ) &&
                        <Icon iconName="FaCaretUp"/>
                      }
                    </span>
                </th>
                <th onClick={this.onSortChange('salary')} className="clickableElement">
                    <span>Salary</span>
                    <span>
                      {
                        this.state.sortCol !== 'salary' &&
                        <Icon iconName="FaSort"/>
                      }
                      {
                        ( this.state.sortCol === 'salary' && !this.state.sortReverse) &&
                        <Icon iconName="FaCaretDown"/>
                      }
                      {
                        (this.state.sortCol === 'salary' && this.state.sortReverse ) &&
                        <Icon iconName="FaCaretUp"/>
                      }
                    </span>
                </th>
                <th onClick={this.onSortChange('date_updated')} className="clickableElement">
                    <span>Updated By </span>
                    <span>
                      {
                        this.state.sortCol !== 'date_updated' &&
                        <Icon iconName="FaSort"/>
                      }
                      {
                        ( this.state.sortCol === 'date_updated' && !this.state.sortReverse) &&
                        <Icon iconName="FaCaretDown"/>
                      }
                      {
                        (this.state.sortCol === 'date_updated' && this.state.sortReverse ) &&
                        <Icon iconName="FaCaretUp"/>
                      }
                    </span>
                </th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               <tr></tr>
               {this.state.data.map(employee => (
               <tr className="clickableElement" key={employee.id} onDoubleClick={() => this.editData(employee)}>
                  <td>{employee.id} </td>
                  <td>{employee.employee_name}</td>
                  <td>{employee.employee_age}</td>
                  <td>{CommonService.numberFormat(employee.employee_salary)} </td>
                  <td>
                    <span>{employee.updated_by} </span>
                    <div className="text-primary-mute"><Moment fromNow>{employee.date_updated}</Moment></div> </td>
                  <td className="chartFeatures"> 
                  <FaTrash onClick={() => this.deleteData(employee)}> Delete </FaTrash>
                     &nbsp;
                     <FaEdit onClick={() => this.editData(employee)}> Edit </FaEdit>
                  </td>
               </tr>
               ))
               }
            </tbody>
         </table>
      </div>
   </div>
   </div>
    )
}
}

const mapStateToProps = state => ({
    loading: state.commonReducer.loading,
    error: state.commonReducer.error,
    entity : state.entityReducer.selectedEntity
});

export default connect(mapStateToProps)(Employee);