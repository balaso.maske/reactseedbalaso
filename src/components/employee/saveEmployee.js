import React, { Component } from "react";
import {connect} from "react-redux";
import * as EmployeeService from "../../services/EmployeeService";
import * as CommonService from "../../services/CommonService";
import { Icon } from '../FontAwesome';
import { SuccessToastr } from '../Toastr';
import  CustomForm from "../Form/form";

class SaveEmployee extends Component{
    constructor(props){
        super(props);
        this.state = {
            isEdit : false,
            pageName: "Employee"
            
        }
        this.saveEmployee = this.saveEmployee.bind(this)
        this.formData = [
          {
            "name": "id",
            "value": "",
            "required": false,
            "regex": "",
            "label": "ID",
            "inputType": "text",
            "errorMsg": "",
            "isEditModeDisplay": true,
            "isDisabled": true
          },
          {
            "name": "name",
            "value": "",
            "required": true,
            "regex": "",
            "label": "Name",
            "inputType": "text",
            "errorMsg": "Please enter valid data"
          },
          {
            "name": "age",
            "value": "",
            "required": true,
            "regex": "",
            "label": "Age",
            "inputType": "number",
            "errorMsg": "Please enter valid data"
          },
          {
            "name": "salary",
            "value": "",
            "required": true,
            "regex": "",
            "label": "Salary",
            "inputType": "number",
            "errorMsg": "Please enter valid data"
          }
        ]
    }

    saveData(data) {
    }

    saveEmployee (employee) { 
        if(this.state.isEdit){
          var data = { employee_name : employee.name,
            employee_age: employee.age, 
            employee_salary : employee.salary,
             id : employee.id, updated_by : this.props.user.email };

            let response = this.props.dispatch(EmployeeService.updateEmployee(data));
              response.then(value => {
                SuccessToastr(" User Updated Successfully id "+ value);
                this.goTo();
              }, reason => {
                console.error(reason); // Error!
              });
        }else{
          let data = { employee_name : employee.name,
            employee_age: employee.age,
            employee_salary : employee.salary,
            updated_by: this.props.user.email }
            let response = this.props.dispatch(EmployeeService.saveEmployee(data));
            response.then(value => {
              SuccessToastr(" User Saved Successfully id "+ value);
              this.goTo();
            }, reason => {
              console.error(reason); // Error!
            });
        }
    }
      goTo(){
        let { transition } = this.props;
        transition.router.stateService.go('employee');
      }
     
      componentWillMount(){
        const { entity } = this.props;
        if(entity && entity.name === "Employee" && entity.data !== undefined){
            const { data} = entity;
            if(data){
                const {id, employee_age, employee_name, employee_salary } = data;
              this.formData.forEach((input) => {
                let name = input["name"];
                  if( name === "name"){
                    input["value"] = employee_name;
                  }else if( name === "salary"){
                    input["value"] = employee_salary;
                  }else if( name === "age"){
                    input["value"] = employee_age;
                  }else if( name === "id"){
                    input["value"] = id;
                  }
              }); 
            }
        }else{
          let object = {
            "name": this.state.pageName,
            "data": undefined
          }
          this.props.dispatch(CommonService.setSelectedEntity(object));
        }
      }
  
    

render() {
      return (
        <div>
        <h3> <a className="link" onClick={() => this.goTo()}> { this.state.isEdit ? 'Update' : 'Add'} Employe  <Icon iconName="FaRegArrowAltCircleLeft"/> </a> </h3>
        <div className="offset-md-2">
           <div className="row">
           
              <div className="col-md-6 col-sm-6 col-xs-12">
              <CustomForm formData={this.formData} onSave={this.saveEmployee} isEdit = {this.state.isEdit} />
              </div>
           </div>
        </div>
     </div>
      )
    }
}

const mapStateToProps = state => ({
    employees: state.employeeReducer.employees,
    entity : state.entityReducer.selectedEntity,
    user : state.commonReducer.user
});
  


export default connect(mapStateToProps)(SaveEmployee);