import React from 'react';
import ReactDOM from 'react-dom';

import store from './store/reducer/rootReducer';
import { Provider } from 'react-redux';
import { router } from './routes/router-config';
import { UIRouter, UIView } from '@uirouter/react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

ReactDOM.render(<Provider store={store}>
  <UIRouter router={router}>
    <div>
      <UIView/>
    </div>
  </UIRouter>
</Provider>,
document.getElementById('root')
);
/*

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

ReactDOM.render(<Provider store={store}>
  <UIRouter router={router}>
      <div>
          <UIView/>
      </div>
  </UIRouter></Provider>,
document.getElementById('root')
);
*/