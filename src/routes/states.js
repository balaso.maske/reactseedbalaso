
import App from '../App';
import Employee from '../components/employee/index';
import SaveEmployee from '../components/employee/saveEmployee';
import Home from '../components/Home';
import Login from '../components/login/login';
import WelcomePage from '../components/WelcomePage';
import TopMenubar from '../components/MenuBar/TopMenuBar';
import User from '../components/user/user';
import UserEntry from '../components/user/userEntry';
import News from '../components/News/news';
import ResetPasswordInit from '../components/ResetPassword/ResetPasswordInit';
import Registration from '../components/Registration/Registration';
import ResetPasswordFinish from '../components/ResetPassword/ResetPasswordFinish';

const app = {
    name: 'app',
    redirectTo: 'home',
    component: App
  };

  const topMenubar = {
    parent: 'home',
    name: 'topMenuBar',
    url: '/home',
    component: TopMenubar
  };

  const user = {
    parent: 'home',
    name: 'user',
    url: '/user',
    component: User
  };

  
  const saveUser = {
    parent: 'home',
    name: 'userEntry',
    url: '/user/userEntry',
    component: UserEntry
  }

  const home = {
    parent: 'app',
    name: 'home',
    url: '/home',
    component: Home
  };

  const welcomepage = {
    parent: 'home',
    name: 'welcomepage',
    url: '/welcomepage',
    component: WelcomePage
  };
 
  const employee = {
    parent: 'home',
    name: 'employee',
    url: '/employee',
    component: Employee
  }
  
  const saveEmployee = {
    parent: 'home',
    name: 'saveEmployee',
    url: '/employee/saveEmployee',
    component: SaveEmployee
  }
 
  const login = {
    parent: 'app',
    name: 'login',
    url: '/login',
    component: Login
  }
  
  const registration = {
    parent: 'app',
    name: 'register',
    url: '/register',
    component: Registration
  }
  
  const news = {
    parent: 'home',
    name: 'news',
    url: '/news',
    component: News
  }

  const resetPassword = {
    parent: 'app',
    name: 'resetPassword',
    url: '/resetPassword',
    component: ResetPasswordInit
  };

  const resetPasswordFinish = {
    parent: 'app',
    name: 'resetPasswordFinish',
    url: '/resetPasswordFinish/:key',
    component: ResetPasswordFinish
  };
  
  export default [app, login, home, employee, saveEmployee, welcomepage, topMenubar, user, news, saveUser, registration, resetPassword, resetPasswordFinish];