import React, { Component } from "react";
import './App.css';
import { UIView } from '@uirouter/react';
import Footer from './components/footer/footer';
import {ToastContainer} from "react-toastify";
import Loader from "react-loader";
import {connect} from "react-redux";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }
  componentDidMount(){
   
  }
  componentWillReceiveProps(){
    
  }

  render() {
    const { isAuthenticated, transition } = this.props;
    if(isAuthenticated === false){
     //transition.router.stateService.go("login");
    }
  return (
    <Loader loaded={this.state.isLoading}>
       <div>
          <UIView/>
          <ToastContainer/>
        </div>
    </Loader>
  )
  };
}

const mapStateToProps = (state) => {
  return {
    loading: state.commonReducer.loading,
    isLeftMenuOpen : state.commonReducer.isLeftMenuOpen,
    isAuthenticated : state.commonReducer.isAuthenticated,
  }
};

export default connect(mapStateToProps, null)(App);
