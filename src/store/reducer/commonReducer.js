import * as actionType from '../action/actionType';
import { ErrorToastr } from '../../components/Toastr';

const initialState = {
    loading: false,
    error: null,
    isAuthenticated: false,
    isLeftMenuOpen: false,
    isAdmin: false,
    isAdminView: false,
    user : ''
};

const commonReducer = (state = initialState, action) => {
    switch (action.type) {
      case actionType.API_CALL_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
      case actionType.API_CALL_END:
        return {
          ...state,
          loading: false,
          error: null
        };
        case actionType.API_CALL_ERROR:
        ErrorToastr("Some Error ocurred : "+ action.payload.error.message);
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
        case actionType.TOGGLE_LEFT_MENU:
          return {
            ...state,
            loading: false,
            isLeftMenuOpen: action.value
          };
        case actionType.IS_USER_AUTHENTICATE:
          return {
            ...state,
            isAuthenticated: action.payload.isAuthenticated,
            isLeftMenuOpen: action.payload.isAuthenticated,
            isAdmin: action.payload.isAdmin,
            isAdminView : action.payload.isAdminView !== undefined ? action.payload.isAdminView  : state.isAdminView,
            user : action.payload.user !== undefined ? action.payload.user  : state.user,
          };
          case actionType.IS_ADMIN_VIEW:
          return {
            ...state,
            isAdminView: action.value
          };
    default:
      return state
    }
};

export default commonReducer;
