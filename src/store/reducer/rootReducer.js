import thunk from "redux-thunk";
import {createStore, combineReducers, applyMiddleware} from "redux";
import employeeReducer from "./employeeReducer";
import commonReducer from "./commonReducer";
import entityReducer from "./entityReducer";
import { SetInStorage, GetFromStorage, getFromStorage, setDataInStorage } from '../../storage/storage';

const rootReducer = combineReducers({ 
  employeeReducer,
  commonReducer,
  entityReducer
});

let dummyData = [
  {
    "id": 1,
    "employee_name": "Balaso Maske",
    "employee_salary": 9999,
    "employee_age": 29,
    "date_updated":"2020-09-09T16:37:28.086Z",
    "updated_by" : "balasomaske@gmail.com"
  },
  {
    "id": 2,
    "employee_name": "Garrett Winters",
    "employee_salary": 170750,
    "employee_age": 63,
    "date_updated":"2020-09-09T16:37:28.086Z",
    "updated_by" : "balasomaske@gmail.com"
  },
  {
    "id": 3,
    "employee_name": "Ashton Cox",
    "employee_salary": 86000,
    "employee_age": 66,
    "date_updated":"2020-09-09T16:37:28.086Z",
    "updated_by" : "balasomaske@gmail.com"
  },
  {
    "id": 4,
    "employee_name": "Cedric Kelly",
    "employee_salary": 433060,
    "employee_age": 22,
    "date_updated":"2020-09-09T16:37:28.086Z",
    "updated_by" : "balasomaske@gmail.com"
  },
  {
    "id": 5,
    "employee_name": "Airi Satou",
    "employee_salary": 162700,
    "employee_age": 33,
    "date_updated":"2020-09-09T16:37:28.086Z",
    "updated_by" : "balasomaske@gmail.com"
  },
  {
    "id": 6,
    "employee_name": "Brielle Williamson",
    "employee_salary": 372000,
    "employee_age": 61,
    "date_updated":"2020-09-09T16:37:28.086Z",
    "updated_by" : "balasomaske@gmail.com"
  }
];
  const dummyEmployees = getFromStorage();
  
  if(dummyEmployees.length === 0){
    setDataInStorage(dummyData);
  }

const persistedState = GetFromStorage();

const store = createStore(rootReducer, persistedState, applyMiddleware(thunk));

store.subscribe(()=>{
  SetInStorage(store.getState());
})

export default store;
