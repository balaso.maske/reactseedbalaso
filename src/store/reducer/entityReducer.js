import * as actionType from '../action/actionType';

const initialState = {
    selectedEntity: null
};

const entityReducer = (state = initialState, action) => {
    switch (action.type) {
      case actionType.SET_ENTITY:
        return {
          ...state,
          selectedEntity: action.value
        };
    default:
      return state
    }
};

export default entityReducer;
