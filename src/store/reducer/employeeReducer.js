import * as actionType from '../action/actionType';

const initialState = {
    loading: false,
    employees : [],
    error: null
};

const employeeReducer = (state = initialState, action) => {
    switch (action.type) {
      case actionType.FETCH_EMPLOYEES:
        return {
          ...state,
          loading: false,
          employees: action.payload.employees
        };
        case actionType.DELETE_EMPLOYEE_BY_ID:
          return {
            ...state,
            loading: false,
            error: null
          };
          case actionType.SAVE_EMPLOYEE:
            return {
              ...state,
              loading: false,
              error: null
            };
    default:
      return state
    }
};

export default employeeReducer;
