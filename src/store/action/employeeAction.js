import * as actionType from './actionType'

export const deleteEmployeeById = id =>({
  type: actionType.DELETE_EMPLOYEE_BY_ID,
  payload: { id }
})

export const saveEmployee = employee => ({
  type: actionType.SAVE_EMPLOYEE,
  payload: { employee }
});
