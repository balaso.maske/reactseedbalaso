import * as actionType from './actionType'

export const APICallBegin = () => ({
    type: actionType.API_CALL_BEGIN
  });
  
  export const APICallEnd = () => ({
    type: actionType.API_CALL_END
  });
  
export const APICallError = error => ({
    type: actionType.API_CALL_ERROR,
    payload: { error }
});
  
export const setEntity = entity => ({
    type: actionType.SET_ENTITY,
    value: entity
});

export const toggleLeftMenu = status => ({
  type: actionType.TOGGLE_LEFT_MENU,
  value: status
});

export const isUserAuthenticate = data => ({
  type: actionType.IS_USER_AUTHENTICATE,
  payload: data
});

export const showAdminView = val => ({
  type: actionType.IS_ADMIN_VIEW,
  value: val
});
  
  
  