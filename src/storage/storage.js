let storageVariable = "reduxStateBalaso1";

let dataStorageVariable = "dummyData1";
let buildNum = "1.0"

export const setDataInStorage = data => {
    localStorage.setItem(dataStorageVariable, JSON.stringify(data))
};
export const getFromStorage = props => {
    return localStorage.getItem(dataStorageVariable) ? JSON.parse(localStorage.getItem(dataStorageVariable)) : [];
};

export const SetInStorage = data => {
   localStorage.setItem(storageVariable, JSON.stringify(data))
};

export const RemoveFromStorage = props => {
    return localStorage.removeItem(storageVariable);
};

export const GetFromStorage = props => {
    return localStorage.getItem(storageVariable) ? JSON.parse(localStorage.getItem(storageVariable)) : [];
};

export function getDataFromStorage (entity){
    return localStorage.getItem( (entity + buildNum) ) ? JSON.parse(localStorage.getItem((entity + buildNum))) : [];
}

export function setDataInStorageV2 (entity, data){
    localStorage.setItem((entity + buildNum), JSON.stringify(data));
}